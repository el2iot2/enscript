'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('underscore');
var util = require('util');

var ERR = {
    EXPECTED_ARRAY: function(got) {
        return new Error('expected [...], but got '+util.inspect(got));
    },
    EXPECTED_ARRAY_HAD_SOME_ELEMENTS: function(got) {
        return new Error('expected [...].length > 0, but got '+util.inspect(got));
    },
    EXPECTED_ARRAY_OR_HASH_OR_STRING: function(got) {
        return new Error('expected [...] or {...} or "...", but got '+util.inspect(got));
    },
    EXPECTED_ARRAY_OR_STRING: function(got) {
        return new Error('expected "..." or [...], but got '+util.inspect(got));
    },
    EXPECTED_FUNCTION_EXISTED: function(got, name) {
        return new Error('expected function "'+name+'", but got '+util.inspect(got));
    },
    EXPECTED_HASH: function(got) {
        return new Error('expected {...}, but got '+util.inspect(got));
    },
    EXPECTED_HASH_OR_STRING: function(got) {
        return new Error('expected {...} or "...", but got '+util.inspect(got));
    },
    EXPECTED_STRING: function(got) {
        return new Error('expected "...", but got '+util.inspect(got));
    },
    EXPECTED_STRING_NOT_NULL_NOR_EMPTY: function(got) {
        return new Error('expected string to be neither null nor empty, but got '+util.inspect(got));
    }
};

//Add the error messages
_.ERR = ERR;

function explain(expected, got) {
    console.warn('Expected '+expected+', Got '+util.inspect(got));
}

//mixin our custom functions
_.mixin({
    expectArray: function (obj) {
        if (!_.isString(obj)) {
            throw ERR.EXPECTED_ARRAY(obj);
        }
    },
    expectArrayHasSomeElements: function (obj) {
        if (!_.isArray(obj) || obj.length <= 0) {            
            throw ERR.EXPECTED_ARRAY_HAD_SOME_ELEMENTS(obj);
        }
    },
    expectArrayOrHashOrString: function (obj) {
        if (!_.isString(obj) && 
            !(_.isObject(obj) && !_.isFunction(obj))) {
            throw ERR.EXPECTED_ARRAY_OR_HASH_OR_STRING(obj);
        }
    },
    expectArrayOrString: function (obj) {
        if (!_.isString(obj) && 
            !(_.isObject(obj) && _.isArray(obj))) {
            throw ERR.EXPECTED_ARRAY_OR_STRING(obj);
        }
    },
    expectFunctionExists: function(obj, functionName) {
        if (!_.has(obj, functionName) || !_.isFunction(obj[functionName])) {
            throw ERR.EXPECTED_FUNCTION_EXISTED(obj, functionName);
        } 
    },
    expectString: function (obj) {
        if (!_.isString(obj)) {
            throw ERR.EXPECTED_STRING(obj);
        }
    },
    expectHashOrString: function (obj) {
        if (!_.isString(obj) && 
            !(_.isObject(obj) && !_.isArray(obj) && !_.isFunction(obj))) {
            throw ERR.EXPECTED_HASH_OR_STRING(obj);
        }
    },
    expectHash: function (obj) {
        if (!(_.isObject(obj) && !_.isArray(obj) && !_.isFunction(obj))) {
            throw ERR.EXPECTED_HASH(obj);
        }
    },
    expectStringNotNullNOrEmpty: function (obj) {
        if (obj === null || obj === '') {
            throw ERR.EXPECTED_STRING_NOT_NULL_NOR_EMPTY(obj);
        }
    },
    eachId: function(obj, thiz, eachFunc, ctx) {
        var item;
        for (var key in obj) {
            if (_.has(obj, key) && key[0] !== '_') {
                item = obj[key];
                if (!_.isFunction(item)) {
                    thiz[eachFunc](key, item, ctx);
                }
            }
        }
    },
    dump: function(object, showHidden, depth) {
        return util.inspect(object, showHidden, depth, true); 
    },
    isSchema: function(object) {
        return object && object.constructor.name === 'Schema';
    },
    isTable: function(object) {
        return object && object.constructor.name === 'Table';
    }
});

//Forward the decorated _
module.exports = _; 
