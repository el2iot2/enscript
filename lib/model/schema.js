'use strict';
/*
 * sqlite-enscript
 * https://github.com/automatonic/sqlite-enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js'),
  Script = require('./script.js');

function Schema(id, obj, ctx) {
  obj = obj || {};
  var meta = obj._meta || {};
  _.defaults(meta, {
    name: id,
    code: id
  });
  this._meta = meta;
  this._ctx = ctx;
  this._id = id;
  this._input = obj; //preserve the original input
  this._sprocs = obj._sprocs;
  ctx.debug('creating new schema with metadata: %j', meta);
  this._releases = [];

  _.eachId(obj, this, 'tableFrom', ctx);

  this.sortTables(ctx);
  this.applyTableConstraints(ctx);
}

//based on https://gist.github.com/1732686
Schema.prototype.sortTables = function(ctx) {
  var tables = {};
  var schema = this;

  //map each table to its references
  _.each(
  this.tables(),

  function(table) {
    var refs = [];
    if (_.isFunction(table._input._references)) {
      refs = table._input._references(schema);
    }
    if (refs && !_.isArray(refs)) {
      refs = [refs];
    }
    tables[table._id] = _.pluck(refs, '_id');
  });
  ctx.debug('sorting tables in schema %j for the following dependencies: %j', this._id, tables);

  schema._sorted = [];
  var visited = {};

  _.each(
  tables,

  function visit(deps, name, list, ancestors) {
    ancestors = ancestors || [];
    ancestors.push(name);
    visited[name] = true;

    _.each(
    deps,

    function(dep) {
      // if already in ancestors, a closed chain exists.
      if (_.indexOf(ancestors, dep) >= 0) {
        throw new Error('Circular dependency "' + dep + '" is required by "' + name + '": ' + ancestors.join(' -> '));
      }

      // if already exists, do nothing
      if (visited[name]) {
        return;
      }
      visit(tables[dep], dep, list, ancestors.slice(0)); // recursive call
    });
    schema._sorted.push(name);
  });
  ctx.debug('sorted tables in schema %j : %j', this._id, schema._sorted);
};

Schema.prototype.applyTableConstraints = function(ctx) {
  var schema = this;
  _.each(
  this._sorted,

  function(tableKey) {
    var table = schema[tableKey];
    if (table && table._input && _.isFunction(table._input._constraints)) {
      table._input._constraints(table, schema);
    }
  });
};

Schema.prototype.extend = function(dest) {
  var extended = _.defaults(dest, this._input);
  extended._meta = _.defaults(extended._meta || {}, this._meta);
  return extended;
};

Schema.prototype.schemaFrom = function(id, obj, ctx) {
  ctx = ctx || this._ctx;
  if (_.isFunction(obj)) {
    var fn = obj;
    var result = fn(this);
    this.schemaFrom(id, result, ctx);
  }
  else {
    _.expectHash(obj);
    ctx.trace('building schema %j from: %j', id, obj);
    var schema = new Schema(id, obj, ctx);
    this[id] = schema;
  }
};

Schema.prototype.releasesFrom = function(obj, ctx) {
  ctx = ctx || this._ctx;
  if (_.isFunction(obj)) {
    var fn = obj;
    var result = fn(this._releases);
    this.releasesFrom(result, ctx);
  }
  else if (_.isArray(obj)) {
    var ary = obj;
    ctx.info('releases from: ' + _.dump(ary, false, 1));
    var i, releaseSchema, releaseId, item;
    for (i = 0; i < ary.length; i += 1) {
      item = ary[i];
      item._meta = item._meta || {};
      releaseId = this._meta.code + '_' + this._releases.length;
      _.defaults(item._meta, {
        name: releaseId,
        code: releaseId,
        version: this._releases.length
      });
      releaseSchema = new Schema(releaseId, item, ctx);
      this._releases.push(releaseSchema);
    }
  }
  else {
    this.releasesFrom([obj], ctx);
  }
};

module.exports = Schema;

var Table = require('./table.js');
Schema.prototype.tableFrom = function(id, obj, ctx) {
  ctx = ctx || this._ctx;
  if (_.isFunction(obj)) {
    var fn = obj;
    var result = fn(this);
    this.tableFrom(id, result, ctx);
  }
  else {
    _.expectArrayOrHashOrString(obj);
    ctx.trace('building table %j from: %j', id, obj);

    var table = new Table(id, obj, ctx);
    //'columnname'
    if (_.isString(obj)) {
      table.columnFrom(obj, null, null);
    }
    else {
      //['columnName']
      //['columnName','INTEGER']
      //['columnName','INTEGER', constraints...]
      //['columnName', constraints...]
      if (_.isArray(obj)) {
        _.expectArrayHasSomeElements(obj);
        table.columnFromArrayOrString(obj[0], _.rest(obj));
      }
      //{ columnName: ..., colName2: ..., ....}
      else {
        _.eachId(obj, table, 'columnFromArrayOrString', ctx);
      }
    }
    this[id] = table;
  }
};

Schema.prototype.getAlter = function(currentSchema, version) {
  if (this._input && _.isFunction(this._input._alter)) {
    var statements = _.flatten(this._input._alter(currentSchema, this));

    return {
      version: version,
      statements: statements
    };
  }
  return null;
};

Schema.prototype.getReleaseAlters = function() {
  var alters = [];
  var currentSchema = this;
  _.each(this._releases, function(release, index) {
    if (release) {
      alters.push(release.getAlter(currentSchema, index));
    }
  });
  return _.compact(alters);
};

Schema.prototype.releases = function() {
  return this._releases;
};

Schema.prototype.pushChildren = function(ary) {
  _.each(this, function(item) {
    if (_.isSchema(item)) {
      ary.push(item);
      item.pushChildren(ary);
    }
  });
  _.each(this._releases, function(item) {
    ary.push(item);
    item.pushChildren(ary);
  });
};

Schema.prototype.tables = function() {
  var results = [];
  _.each(this, function(item) {
    if (_.isTable(item)) {
      results.push(item);
    }
  });
  return results;
};

Schema.prototype.tableKeys = function() {
  var results = [];
  _.each(this, function(item, key) {
    if (_.isTable(item)) {
      results.push(key);
    }
  });
  return results;
};

Schema.prototype.getDropTableStatement = function(tableName, databaseName) {
  var stmt = ['DROP TABLE',
  databaseName ? databaseName + '.' + tableName : tableName];
  return stmt.join(' ');
};

Schema.prototype.enscriptAlterFrom = function(from, dir, name) {
  var script = new Script([],
  dir,
  name || 'alter_' + from.id + '_' + this.id);
  return script;
};

Schema.prototype.getCreateStatements = function(excludeVersionPragma, databaseName) {
  var lines = [];
  if (_.isNumber(this._meta.version) && !excludeVersionPragma) {
    lines.push('PRAGMA user_version = ' + this._meta.version);
  }
  var schema = this;
  _.each(this._sorted,

  function(tableKey) {
    var table = schema[tableKey];
    table.enscriptCreate(databaseName, lines);
  });

  //Allow a custom override on the schema
  if (_.isFunction(this._input._create)) {
    lines = _.flatten(this._input._create(lines));
  }

  return lines;
};

Schema.prototype.enscriptCreate = function(dir, name, excludeVersionPragma, databaseName) {
  var lines = this.getCreateStatements(excludeVersionPragma, databaseName);

  var script = new Script(
  lines,
  dir,
  name || 'create_' + this._meta.code);
  return script;
};