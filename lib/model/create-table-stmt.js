'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');

//http://www.sqlite.org/syntaxdiagrams.html#create-table-stmt
function CreateTableStmt(databaseName, tableName, columnDefs, tableConstraints, asSelect, temp, ifNotExists) {
  this.columnDefs = columnDefs;
  this.tableConstraints = tableConstraints;
  this.ine = ifNotExists;
  this.temp = temp;

  if (!columnDefs && !asSelect) {
    throw new Error('expected either columns or AS SELECT');
  }

  if (tableConstraints && !columnDefs) {
    throw new Error('expected columns before table constraints');
  }

  this.asSelectFunc = asSelect;
  _.expectStringNotNullNOrEmpty(tableName);
  this.tableName = tableName;

  if (databaseName) {
    _.expectString(databaseName);
  }
  this.databaseName = databaseName;
}

//Copy (to avoid mutation) to a new statement, but include ine flag
CreateTableStmt.prototype.IF_NOT_EXISTS = function() {
  return new CreateTableStmt(this.databaseName,
  this.tableName,
  this.columnDefs,
  this.tableConstraints,
  this.asSelect,
  this.temp, true);
};

//Copy (to avoid mutation) to a new statement, but include ine flag
CreateTableStmt.prototype.TEMP = function() {
  return new CreateTableStmt(this.databaseName,
  this.tableName,
  this.columnDefs,
  this.tableConstraints,
  this.asSelect,
  true, this.ine);
};

CreateTableStmt.prototype.TEMPORARY = CreateTableStmt.prototype.TEMP;

//Enscript and return
function selectText(enscriptable) {
  return enscriptable.enscript();
}

CreateTableStmt.prototype.enscript = function(lines) {
  lines.push(this.getStatement());
};

CreateTableStmt.prototype.getStatement = function() {

  var stmt = ['CREATE'];

  if (this.temp) {
    stmt.push('TEMP'); //prefer shortened for space?
  }

  stmt.push('TABLE');

  if (this.databaseName) {
    stmt.push(this.databaseName + '.' + this.tableName);
  }
  else {
    stmt.push(this.tableName);
  }

  if (this.ine) {
    stmt.push('IF NOT EXISTS');
  }

  if (this.asSelect) {
    stmt.push(this.asSelect.enscript());
  }
  else if (_.any(this.columnDefs)) {

    var details = _.chain(this.columnDefs).map(selectText);

    if (_.any(this.tableConstraints)) {
      details = details.concat(_.map(this.tableConstraints, selectText));
    }

    stmt.push('(' + details.value().join(', ') + ')');
  }
  else {
    throw new Error('invalid create_table_stmt. must either define columns or use AS_SELECT');
  }

  return stmt.join(' ');
};

//Export constructor
module.exports = CreateTableStmt;
