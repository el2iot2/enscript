'use strict';
/*
 * sqlite-enscript
 * https://github.com/automatonic/sqlite-enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */
 
var _ = require('./ctx.js');

function Table(id, obj, ctx) {
    obj = obj || {};
    var meta = obj._meta || {};
    _.defaults(meta, {name: id, code: id});
    ctx.debug('creating new table with metadata: %j', meta);
    this._meta = meta;
    this._id = id;
    this._ctx = ctx;
    this._input = obj; //preserve the original input
    
    this._columnDefs = [];
    this._tableConstraints = [];
}

Table.prototype.pushColumnDef = function(columnDef) {
    this._columnDefs.push(columnDef);
    this[columnDef.columnName] = columnDef;
};

var CreateTableStmt = require('./create-table-stmt.js');
Table.prototype.create = function(databaseName) {
    var stmt = new CreateTableStmt(databaseName,
        this._id,
        this._columnDefs, 
        this._tableConstraints);
    return stmt;
};

Table.prototype.enscriptCreate = function(databaseName, lines) {
    this.create(databaseName).enscript(lines);
};

Table.prototype.getCreateStatement = function(databaseName) {
    return this.create(databaseName).getStatement();
};

Table.prototype.getRenameToStatement = function(newName, databaseName) {
    var stmt = ['ALTER TABLE',
        databaseName ? databaseName + '.' + this._id : this._id,
        'RENAME TO',
        newName
    ];
    return stmt.join(' ');
};

Table.prototype.alter = function(target) {
};

module.exports = Table;
var TypeName = require('./type-name.js');
var ColumnDef = require('./column-def.js');
Table.prototype.columnFrom = function(name, typeName, obj) {
    _.expectString(name);
    
    obj = obj || [];
    _.expectArrayOrString(obj);
    
    if (_.isString(obj)) {
        obj = [obj];
    }
    obj = _.clone(obj);
    if (!typeName && 
        obj.length > 0) {
        typeName = TypeName.parse(obj[0]);
        if (typeName) {
            obj.shift();
        }
    }
    
    var columnDef = new ColumnDef(this, name, typeName);
    this.pushColumnDef(columnDef);
    
    for (var i = 0; i < obj.length; i+=1) {
        columnDef.columnConstraintFrom(obj[i]);
    }
};

Table.prototype.columnFromArrayOrString = function (name, obj) {
    var typeName = null;

    _.expectArrayOrString(obj);
    if (_.isString(obj)) {
        obj = [obj];
    }
    
    var constraints = obj || [];
    constraints = _.clone(constraints);
    if (constraints.length > 0) {
        typeName = TypeName.parse(constraints[0]);
        if (typeName) {
            constraints.shift();
        }
    }
    this.columnFrom(name, typeName, constraints); 
};

var TableConstraintForeignKeyClause = require('./table-constraint-foreign-key-clause.js');
Table.prototype.FOREIGN_KEY = function(columnDefs) {
    if (!_.isArray(columnDefs)) {
        columnDefs = [columnDefs];
    }
    var tableConstraint = new TableConstraintForeignKeyClause(columnDefs);
    this._tableConstraints.push(tableConstraint);
    return tableConstraint;
};

var TableConstraintConflictClause = require('./table-constraint-conflict-clause.js').ctors;
Table.prototype.UNIQUE = function(indexedColumns) {
    if (!_.isArray(indexedColumns)) {
        indexedColumns = [indexedColumns];
    }
    var tableConstraint = TableConstraintConflictClause.UNIQUE(indexedColumns);
    this._tableConstraints.push(tableConstraint);
    return tableConstraint;
};

Table.prototype.PRIMARY_KEY = function(indexedColumns) {    
    if (!_.isArray(indexedColumns)) {
        indexedColumns = [indexedColumns];
    }
    var tableConstraint = TableConstraintConflictClause.PRIMARY_KEY(indexedColumns);
    this._tableConstraints.push(tableConstraint);
    return tableConstraint;
};
