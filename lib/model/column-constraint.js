'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');
//var util = require('util');

//http://www.sqlite.org/syntaxdiagrams.html#column-constraint
function ColumnConstraint(name, _constraint) {
    this.name = name;
    this._constraint = _constraint;
    if (!this._constraint) {
        throw new Error('no constraint body was defined');
    }
}

ColumnConstraint.prototype.enscript = function() {
    var stmt = [];
    
    if (this.name) {
        stmt.push('CONSTRAINT');
        stmt.push(this.name);
    }

    stmt.push(this._constraint.enscript());
    
    return stmt.join(' ');
};

var ColumnConstraintDefaultClause = require('./column-constraint-default.js');
exports.DEFAULT = function(obj) {
    return new ColumnConstraint(null, new ColumnConstraintDefaultClause(obj));
};

exports.CONSTRAINT_DEFAULT = function(name, obj) {
    return new ColumnConstraint(name, new ColumnConstraintDefaultClause(obj));
};

//turns sub.PRIMARY_KEY(...) -> exports.PRIMARY_KEY(...)
//that creates a column constraint and returns it
function wrapAnonymous(key, innerCtor) {
    var args = arguments;
    exports[key] = _.wrap(innerCtor, function(ctor) {
        return new ColumnConstraint(null, ctor(args));
    });
}

//turns sub.PRIMARY_KEY(...) -> exports.CONSTRAINT_PRIMARY_KEY(name,...)
//that creates a named column constraint and returns it
function wrapNamed(key, innerCtor) {
    var args = arguments;
    exports['CONSTRAINT_'+key] = _.wrap(innerCtor, function composeCtor(ctor) {
        return new ColumnConstraint(_.first(args), ctor(_.rest(args)));
    });
}

//Copy exports from constraint submodule for primary key
var pk = require('./column-constraint-primary-key.js');
for (var key in pk.ctors) {
    if (pk.ctors.hasOwnProperty(key)) {
        wrapAnonymous(key,pk.ctors[key]);
        wrapNamed(key,pk.ctors[key]);
    }
}

//Copy exports from constraint submodule for conflict clauses
var cc = require('./column-constraint-conflict-clause.js');
for (key in cc.ctors) {
    if (cc.ctors.hasOwnProperty(key)) {
        wrapAnonymous(key,cc.ctors[key]);
        wrapNamed(key,cc.ctors[key]);
    }
}

