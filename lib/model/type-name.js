'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');

var AFFINITY = { 
    INTEGER: {
        INT: 'INT',
        INTEGER: 'INTEGER',
        TINYINT: 'TINYINT',
        SMALLINT: 'SMALLINT',
        MEDIUMINT: 'MEDIUMINT',
        BIGINT: 'BIGINT',
        UNSIGNED_BIG_INT: 'UNSIGNED BIG INT',
        INT2: 'INT2',
        INT8: 'INT8'
    },
    TEXT: {
        CHARACTER: 'CHARACTER',
        VARCHAR: 'VARCHAR',
        VARYING_CHARACTER: 'VARYING CHARACTER',
        NCHAR: 'NCHAR',
        NATIVE_CHARACTER: 'NATIVE CHARACTER',
        NVARCHAR: 'NVARCHAR',
        TEXT: 'TEXT',
        CLOB: 'CLOB'
    },
    NONE: {
        BLOB: 'BLOB'
    },
    REAL: {
        REAL: 'REAL',
        DOUBLE: 'DOUBLE',
        DOUBLE_PRECISION: 'DOUBLE PRECISION',
        FLOAT: 'FLOAT'
    },
    NUMERIC: {
        NUMERIC: 'NUMERIC',
        DECIMAL: 'DECIMAL',
        BOOLEAN: 'BOOLEAN',
        DATE: 'DATE',
        DATETIME: 'DATETIME'
    }
};

//copy and flatten into a type array
exports.TYPES = _.extend({}, 
    AFFINITY.INTEGER, 
    AFFINITY.NONE, 
    AFFINITY.NUMERIC, 
    AFFINITY.REAL, 
    AFFINITY.TEXT);

var typeRegExp = /(\bINT(?:2|8|EGER)?\b|\bTINYINT\b|\bSMALLINT\b|\bMEDIUMINT\b|\bBIGINT\b|\bUNSIGNED\s+BIG\s+INT\b|\bCHARACTER\b|\bVARCHAR\b|\bVARYING\s+CHARACTER\b|\bNCHAR\b|\bNATIVE\s+CHARACTER\b|\bNVARCHAR\b|\bTEXT\b|\bCLOB\b|\bBLOB\b|\bREAL\b|\bDOUBLE(?:\s+PRECISION)?\b|\bFLOAT\b|\bNUMERIC\b|\bDECIMAL\b|\bBOOLEAN\b|\bDATE(?:TIME)?\b)(\s*\(\s*[+\-]?[0-9]+(\s*,\s*[+\-]?[0-9]+\s*)?\))?/i;

//http://www.sqlite.org/syntaxdiagrams.html#type-name
function TypeName(typeName, dim1, dim2) {
    if (!typeName) {
        throw new Error('type must have a name');
    }
    this.typeName = typeName.toUpperCase().trim().replace(/\s+/g, ' ');
    this.dim1 = dim1 ? dim1.trim() : null;
    this.dim2 = dim2 ? dim2.trim() : null;
}

TypeName.prototype.enscript = function() {
    var stmt = this.typeName;
    
    if (this.dim1) {
        if (this.dim2) {
            stmt += '('+this.dim1 + ', '+this.dim2+')';
        }
        else {
            stmt += '('+this.dim1 + ')';
        }
    }
    
    return stmt;
};

//Export constructor
exports.parse = function(name) {
    var matches = typeRegExp.exec(name);
    if (matches) {
        return new TypeName(matches[1],matches[2],matches[3]);
    }
    return null;
};

//export match
exports.exec = function(name) {
    return typeRegExp.exec(name);
};

