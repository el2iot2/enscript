'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var Enumerable = require('linq');

//http://www.sqlite.org/syntaxdiagrams.html#sql-stmt-list
function SqlStmtList(stmtFunc) {
    this.sqlStmts = Enumerable.Empty();
    if (stmtFunc) {
        stmtFunc.call(this);
    }
}

SqlStmtList.prototype.enscript = function() {
    if (this.sqlStmts.Any()) {
        return this.sqlStmts
            .Select(function selectText(s) {
                return s.enscript();
            })
            .ToString(';\r\n')+';';
    }
    else {
        return '';
    }
};

//Copy over the sql statment methods for use here
var stmt = require('./sql-stmt.js');

function copyFunction(f) {
    return function() {
        var sqlStmt = f.apply(this, arguments);
        this.sqlStmts = this.sqlStmts.Concat(Enumerable.Return(sqlStmt));
        return this;
    };
}

for (var key in stmt) {
    if (stmt.hasOwnProperty(key)) {
        SqlStmtList.prototype[key] = copyFunction(stmt[key]);
    }
}

//Export constructor
module.exports = SqlStmtList;