'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var CreateTableStmt = require('./create-table-stmt.js');

exports.CREATE_TABLE = function(name, columnDefFunc, tableConstraintFunc) {
    return new CreateTableStmt(name, columnDefFunc, tableConstraintFunc, null, null, null);
};

exports.CREATE_TEMP_TABLE = function(name, columnDefFunc, tableConstraintFunc, asSelectFunc) {
    return new CreateTableStmt(name, columnDefFunc, tableConstraintFunc, asSelectFunc, true, null);
};

exports.CREATE_TEMPORARY_TABLE = exports.CREATE_TEMP_TABLE;

exports.CREATE_TABLE_AS_SELECT = function(name, asSelectFunc) {
    return new CreateTableStmt(name, null, null, asSelectFunc, null, null);
};

exports.CREATE_TEMP_TABLE_AS_SELECT = function(name, asSelectFunc) {
    return new CreateTableStmt(name, null, null, asSelectFunc, true, null);
};

exports.CREATE_TEMPORARY_TABLE_AS_SELECT = exports.CREATE_TEMP_TABLE_AS_SELECT;