'use strict';
/*
 * sqlite-enscript
 * https://github.com/automatonic/sqlite-enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');

function Schemata(meta, ctx) {
  ctx.debug('creating new schemata with metadata: %j', meta);
  this._meta = meta;
  this._ctx = ctx;
}

module.exports = Schemata;

var Schema = require('./schema.js');
Schemata.prototype.schemaFrom = function(id, obj, ctx) {
  ctx = ctx || this._ctx;

  _.expectHash(obj);

  ctx.trace('building schema %j from: %j', id, obj);

  var schema = new Schema(id, obj, ctx);
  this[id] = schema;
};

Schemata.prototype.schemas = function() {
  var results = [];
  _.each(this, function(item) {
    if (_.isSchema(item)) {
      results.push(item);
      item.pushChildren(results);
    }
  });
  return results;
};
