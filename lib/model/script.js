'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');
var fs = require('q-io/fs');
var path = require('path');

function Script(statements, dir, name) {
  this.name = name || 'script';
  this.statements = statements || [];
  this.dir = dir || __dirname;
}

function requireDirectory(forPath) {
  var dir = path.dirname(forPath);
  return fs.stat(dir).then(

  function(stat) {
    if (!stat.isDirectory()) {
      throw new Error('"' + dir + '" exists, but is not a directory');
    }
  },

  function(error) {
    return fs.makeDirectory(forPath);
  });
}

Script.prototype.writeFile = function(targetPath) {
  var lines = this.statements.join(';\r\n');
  if (lines.trim().length > 0) {
    lines += ';';
  }
  else {
    console.warn("no data for " + this.name);
  }
  targetPath = targetPath || path.resolve(this.dir, this.name + '.sql');

  return requireDirectory(targetPath).then(function() {
    return fs.writeFile(targetPath, lines).then(function() {
      return targetPath;
    },

    function(error) {
      throw error;
    });
  },

  function(error) {
    throw error;
  });
};

var tableRegEx = /^CREATE\s+TABLE\s+(IF\s+NOT\s+EXISTS\s+)?([a-zA-Z0-9_]+).*$/;

Script.prototype.getCreateTableNames = function() {
  //map to name
  function getCreateTableName(statement) {
    var match = tableRegEx.exec(statement);
    if (match !== null) {
      return match[2] || "!error!";
    }
    return null;
  }
  //
  return _.chain(this.statements).map(getCreateTableName).compact().value();
};

//Export constructor
module.exports = Script;
