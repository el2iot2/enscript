'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');
//var util = require('util');

//http://www.sqlite.org/syntaxdiagrams.html#column-def
function ColumnDef(table, columnName, typeName, columnConstraintFunc) {
    if (!(_.isString(columnName))) {
        throw new Error('expected column name was a string');
    }
    
    columnName = columnName.trim();
    
    if (columnName.length === 0) {
        throw new Error('column name is too short');
    }
    this.table = table;
    this.columnName = columnName;
    this.typeName = typeName;
    this.columnConstraints = [];
}

//Export constructor
module.exports = ColumnDef;


//Enscript and return
function selectText(enscriptable) {
    return enscriptable.enscript();
}

ColumnDef.prototype.enscript = function() {
    var stmt = [this.columnName];
    
    if (this.typeName) {
        stmt.push(this.typeName.enscript());
    }
    
    if (_.any(this.columnConstraints)) {     
        stmt.push( _.map(this.columnConstraints,selectText));
    }
    return _.flatten(stmt).join(' ');
};

ColumnDef.prototype.pushColumnConstraint = function(columnConstraint) {
    this.columnConstraints.push(columnConstraint);
};

function tableConstraintFrom(columnDef, obj) {}

var ColumnConstraint = require('./column-constraint.js');
ColumnDef.prototype.columnConstraintFrom = function(obj) {
    _.expectStringNotNullNOrEmpty(obj);
    
    var columnConstraint = null;
    //Detect named constraint
    
    var matches = /\s*(?:CONSTRAINT\s+(\w+)\s+)?(PRIMARY\s+KEY|NOT\s+NULL|UNIQUE|CHECK|DEFAULT|COLLATE|REFERENCES)(.+)?$/i.exec(obj);
    if (!matches) {
        throw new Error('invalid column constraint format: '+ obj);
    }
    var constraint = { name: matches[1], directive: matches[2].trim().replace(/\s+/g, ' '), remainder: matches[3], funcName: null };
        
    //is there a constraint name?
    if (constraint.name) { 
        //'CONSTRAINT t PRIMARY KEY'->'CONSTRAINT_PRIMARY_KEY('t')'
        constraint.funcName = 'CONSTRAINT_'+constraint.directive.replace(/\s+/g, '_');
        constraint.parm1 = constraint.name;
        constraint.parm2 = constraint.remainder;
    }
    else {
        //'PRIMARY KEY'->'PRIMARY_KEY()'
        constraint.funcName = constraint.directive.replace(/\s+/g, '_');
        constraint.parm1 = constraint.remainder;
        constraint.parm2 = null;
    }
    
    _.expectFunctionExists(ColumnConstraint, constraint.funcName);
    
    columnConstraint = ColumnConstraint[constraint.funcName](constraint.parm1, constraint.parm2);
    this.pushColumnConstraint(columnConstraint);
};



