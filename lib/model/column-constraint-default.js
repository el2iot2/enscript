'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

function ColumnConstraintDefaultClause(payload) {
    this.payload = payload.trim();
}

ColumnConstraintDefaultClause.prototype.enscript = function() {
    return ['DEFAULT', this.payload].join(' ');
};

module.exports = ColumnConstraintDefaultClause;