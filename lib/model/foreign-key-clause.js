'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');

function ForeignKeyClause(foreignTable, columnNames) {
    this.columnNames = columnNames;
    this.foreignTable = foreignTable;
}

ForeignKeyClause.prototype.enscript = function() {
    var stmt = ['REFERENCES', this.foreignTable._id];
    
    if (_.any(this.columnNames)) {
        stmt.push('('+this.columnNames.join(', ')+')');
    }
    
    return stmt.join(' ');
};

module.exports = ForeignKeyClause;

////SORTS
//var ASC = 'ASC',
//    DESC = 'DESC';
//
//var SORTS = [
//    null,
//    ASC,
//    DESC
//    ];
//    
//exports.sorts = function() {
//    return SORTS;
//};
//
////AUTOINCREMENTS
//var AUTOINCREMENT = 'AUTOINCREMENT';
//
//var AUTOINCREMENTS = [
//    null,
//    AUTOINCREMENT
//];
//
//exports.autoincrements = function() {
//    return AUTOINCREMENTS;
//};
//
//exports.ctors = {};
//
////build an exported constructor for each valid combination of prefix and 
////conflict clause
//function buildCtor(sort, conflictClause, autoincrement) {
//    var id = ['PRIMARY_KEY'];
//    var conflictAction = null;
//    
//    if (sort) {
//        id.push(sort);
//    }
//    
//    if (conflictClause) {
//        id.push(conflictClause.id);
//        conflictAction = conflictClause.action;
//    }
//    
//    if (autoincrement) {
//        id.push(autoincrement);
//    }
//    
//    exports.ctors[id.join('_')] = function() {
//        return new ColumnConstraintPrimaryKey(sort, conflictAction, autoincrement);    
//    };
//}
//
//var CONFLICT_CLAUSES = require('./conflict-clause.js').conflictClauses();
//for (var s = 0; s < SORTS.length; s+=1) {
//    for (var cc = 0; cc < CONFLICT_CLAUSES.length; cc+=1) {
//        for(var ai = 0; ai < AUTOINCREMENTS.length; ai += 1) {
//            buildCtor(SORTS[s], CONFLICT_CLAUSES[cc], AUTOINCREMENTS[ai]);    
//        }
//    }
//}
