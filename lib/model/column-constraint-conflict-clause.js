'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

function ColumnConstraintConflictClause(pre, conflictAction) {
    this.pre = pre;
    this.conflictAction = conflictAction;
}

ColumnConstraintConflictClause.prototype.enscript = function() {
    var stmt = [this.pre];
    
    if (this.conflictAction) {
        stmt.push('ON CONFLICT');
        stmt.push(this.conflictAction);
    }
    return stmt.join(' ');
};

//Pre(fixes)
var NOT_NULL = 'NOT NULL',
    UNIQUE = 'UNIQUE';

var PREFIXES = [
    {id:'NOT_NULL', text:NOT_NULL},
    {id:'UNIQUE', text:UNIQUE}
    ];
    
exports.prefixes = function() {
    return PREFIXES;
};

exports.ctors = {};

//build an exported constructor for each valid combination of prefix and 
//conflict clause
function buildCtor(pre, conflictClause) {
    var id = pre.id;
    var conflictAction = null;
    if (conflictClause) {
        id = id + '_' + conflictClause.id;
        conflictAction = conflictClause.action;
    }
    exports.ctors[id] = function() {
        return new ColumnConstraintConflictClause(pre.text, conflictAction);    
    };
}

var CONFLICT_CLAUSES = require('./conflict-clause.js').conflictClauses();
for (var p = 0; p < PREFIXES.length; p+=1) {
    for (var cc = 0; cc < CONFLICT_CLAUSES.length; cc+=1) {
        buildCtor(PREFIXES[p], CONFLICT_CLAUSES[cc]);
    }
}
