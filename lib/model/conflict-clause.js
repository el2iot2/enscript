'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

//http://www.sqlite.org/syntaxdiagrams.html#conflict-clause

//Conflict Actions
var ROLLBACK = 'ROLLBACK',
    ABORT = 'ABORT',
    FAIL = 'FAIL',
    IGNORE = 'IGNORE',
    REPLACE = 'REPLACE';

var CONFLICT_ACTIONS = [
    null,
    ROLLBACK,
    ABORT,
    FAIL,
    IGNORE,
    REPLACE
];

var ON_CONFLICT = 'ON CONFLICT';
var ON_CONFLICT_ = 'ON_CONFLICT_';

var CONFLICT_CLAUSES = [
    null,
    {id: ON_CONFLICT_ + ROLLBACK, action:ROLLBACK, text:ON_CONFLICT + ROLLBACK},
    {id: ON_CONFLICT_ + ABORT,    action:ABORT,    text:ON_CONFLICT + ABORT},
    {id: ON_CONFLICT_ + FAIL,     action:FAIL,     text:ON_CONFLICT + FAIL},
    {id: ON_CONFLICT_ + IGNORE,   action:IGNORE,   text:ON_CONFLICT + IGNORE},
    {id: ON_CONFLICT_ + REPLACE,  action:REPLACE,  text:ON_CONFLICT + REPLACE}
];

exports.conflictActions = function () {
    return CONFLICT_ACTIONS;
};

exports.conflictClauses = function () {
    return CONFLICT_CLAUSES;
};