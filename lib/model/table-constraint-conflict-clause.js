'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');

function TableConstraintConflictClause(pre, indexedColumns) {
    this.pre = pre;
    this.indexedColumns = indexedColumns;
}

TableConstraintConflictClause.prototype.enscript = function() {
    var stmt = [this.pre];
    
    var indexedColumns = _.chain(this.indexedColumns).pluck('columnName');
                
    stmt.push('('+indexedColumns.value().join(', ')+')');

    if (this.conflictAction) {
        stmt.push('ON CONFLICT');
        stmt.push(this.conflictAction);
    }
    
    return stmt.join(' ');
};

//Pre(fixes)
var PRIMARY_KEY = 'PRIMARY KEY',
    UNIQUE = 'UNIQUE';

var PREFIXES = [
    {id:'PRIMARY_KEY', text:PRIMARY_KEY},
    {id:'UNIQUE', text:UNIQUE}
    ];
    
exports.prefixes = function() {
    return PREFIXES;
};

exports.ctors = {};

//build an exported constructor for each valid combination of prefix and 
//conflict clause
function buildCtor(pre, conflictClause) {
    var id = pre.id;
    exports.ctors[id] = function(indexedColumns) {
        return new TableConstraintConflictClause(pre.text, indexedColumns);    
    };
}

//build an exported method for each valid conflict clause
function buildConflictClauseMethod(conflictClause) {
    if (conflictClause) {
        var id = conflictClause.id;
        TableConstraintConflictClause.prototype[id] = function() {
            this.conflictAction = conflictClause.action;    
        };
    }
}

for (var p = 0; p < PREFIXES.length; p+=1) {
    buildCtor(PREFIXES[p]);
}

var CONFLICT_CLAUSES = require('./conflict-clause.js').conflictClauses();
_.each(CONFLICT_CLAUSES, buildConflictClauseMethod);

