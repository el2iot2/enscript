'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./ctx.js');

function TableConstraintForeignKeyClause(columnDefinitions) {
    this.columnNames = _.pluck(columnDefinitions || [], 'columnName');
    this.fkc = null;
}

TableConstraintForeignKeyClause.prototype.enscript = function() {
    var stmt = ['FOREIGN KEY'];
    
    stmt.push('('+this.columnNames.join(', ')+')');

    if (!this.fkc) {
        throw new Error('Expected a REFERENCES statement');
    }
    stmt.push(this.fkc.enscript());
    
    return stmt.join(' ');
};

//Export constructor
module.exports = TableConstraintForeignKeyClause;

var ForeignKeyClause = require('./foreign-key-clause.js');
TableConstraintForeignKeyClause.prototype.REFERENCES = function(columnDefs) {
    
    if (!_.isArray(columnDefs)) {
        columnDefs = [columnDefs];
    }
    
    if (columnDefs.length < 1) {
        throw new Error('Expected a table reference and at least one column def');
    }
    
    
    var foreignTable = _.pluck(columnDefs, 'table')[0];
    
    var columnNames = _.pluck(columnDefs, 'columnName');
    this.fkc = new ForeignKeyClause(foreignTable, columnNames);
};