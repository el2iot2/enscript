'use strict';
/*
 * enscript
 * https://github.com/automatonic/enscript
 *
 * Copyright (c) 2012 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var _ = require('./model/ctx.js');
var fn = require('when/function');
var Schemata = require('./model/schemata.js');
var Script = require('./model/script.js');

function schemataFrom(obj, opt) {
    _.expectHash(obj);
    
    var ctx = opt || require('logule').init(module, 'enscript');
    
    var schemata = new Schemata(obj._meta, ctx);
    
    _.eachId(obj, schemata, 'schemaFrom', ctx);
    return schemata;
}

exports.schemataFrom = function(obj, opt) {
    return fn.call(schemataFrom, obj, opt);
};

exports.scriptFrom = function(statements, name, dir) {
    return new Script(statements, dir, name);  
};

