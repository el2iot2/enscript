'use strict';

var lib = require('./lib/enscript.js');
var when = require('when');
var _ = require("./lib/model/ctx.js");
var TypeName = require("./lib/model/type-name.js");
var util = require('util');
        
function itCreates(obj, expectedSql, parentLog) {
  var log = parentLog.sub(util.format('%j',obj));
  log.info('as %j', expectedSql);
  return lib
    .schemataFrom(obj, log)
    .then(function(schemata) {
      return schemata.sch.tbl.create().getStatement().should.equal(expectedSql);
    });
}

function precepts(O) {
  return {
    rejects : function() {
      var parentLog = this;
      function itRejects(obj, errName) {
        var log = parentLog.sub(util.format('%j',obj));
        log.info('with %j', errName);
        return lib.schemataFrom(obj, log).should.be.rejected;
      }

      return when.all([
        itRejects(null, "EXPECTED_HASH"),
        itRejects([], "EXPECTED_HASH"),
        itRejects({sch : null}, "EXPECTED_HASH"),
        itRejects({sch : []}, "EXPECTED_HASH"),
        itRejects({"sch.sub" : null}, "EXPECTED_HASH"),
        itRejects({"sch.subs" : []}, "EXPECTED_HASH"),
        itRejects({"sch.sub.sub2" : null}, "EXPECTED_HASH"),
        itRejects({"sch.sub.sub2" : []}, "EXPECTED_HASH"),        
        itRejects({sch : {tbl: null}}, "EXPECTED_ARRAY_OR_HASH_OR_STRING"),
        itRejects({sch : {tbl: []}}, "EXPECTED_ARRAY_HAD_SOME_ELEMENTS"),
        itRejects({"sch.sub" : {tbl: null}}, "EXPECTED_ARRAY_OR_HASH_OR_STRING"),
        itRejects({"sch.sub" : {tbl: []}}, "EXPECTED_ARRAY_HAD_SOME_ELEMENTS"),
        itRejects({"sch.sub.sub2" : {tbl: null}}, "EXPECTED_ARRAY_OR_HASH_OR_STRING"),
        itRejects({"sch.sub.sub2" : {tbl: []}}, "EXPECTED_ARRAY_HAD_SOME_ELEMENTS")
        ])
        .otherwise(function(err) {
          parentLog.error("test %j",err);
        });
    },
    creates : function() {
      var hoc = this;
      var promises = [
        itCreates({sch: {tbl : "col"}}, "CREATE TABLE tbl (col)", hoc),
        itCreates({sch: {tbl : {col: "double"}}}, "CREATE TABLE tbl (col DOUBLE)", hoc),
        itCreates({sch: {tbl : {col: "DOUBLE  PRECISION"}}}, "CREATE TABLE tbl (col DOUBLE PRECISION)", hoc),
        itCreates({sch: {tbl : {col: "PRIMARY KEY"}}}, "CREATE TABLE tbl (col PRIMARY KEY)", hoc),
        //itCreates({sch: {tbl : {col: "CONSTRAINT cnst PRIMARY KEY"}}}, "CREATE TABLE tbl (col CONSTRAINT cnst PRIMARY KEY)", hoc),
        itCreates({sch: {tbl : {col: ["int", "DEFAULT 6"]}}}, "CREATE TABLE tbl (col INT DEFAULT 6)", hoc)
      ];
      _.each(TypeName.TYPES, function(type) {
        promises.push(itCreates({sch: {tbl : {col: type}}}, "CREATE TABLE tbl (col "+type+")", hoc));    
      });
      return when.all(promises);
    },
    all : ['rejects', 'creates']
  };
}

module.exports = function(cadfael) {
  return cadfael
    .observantFor(module, precepts)
    .withChaiShould();
};


