/*global describe:true it:true expect:true */

var enscript = require('../lib/enscript.js'),
    util = require("util"),
    _ = require("../lib/model/ctx.js"),
    TypeName = require("../lib/model/type-name.js");

describe("enscript", function () {
    describe("schemataFrom", function () {
        function itRejects(obj, errName) {
            it("rejects " + util.inspect(obj), function() {
                expect(function() {
                    enscript.schemataFrom(obj);
                }).toThrow(_.ERR[errName]());
            });
        }
        
        function itCreates(obj, expectedSql) {
            it("supports " + util.inspect(obj, false, 5) + " -> " + expectedSql, function() {
                var schemata = enscript.schemataFrom(obj);
                var sql = schemata.sch.tbl.create().enscript();
                expect(sql).toEqual(expectedSql);
            });
        }
        
        itRejects(null, "EXPECTED_HASH");
        itRejects([], "EXPECTED_HASH");
        itRejects({sch : null}, "EXPECTED_HASH");
        itRejects({sch : []}, "EXPECTED_HASH");
        itRejects({"sch.sub" : null}, "EXPECTED_HASH");
        itRejects({"sch.sub" : []}, "EXPECTED_HASH");
        itRejects({"sch.sub.sub2" : null}, "EXPECTED_HASH");
        itRejects({"sch.sub.sub2" : []}, "EXPECTED_HASH");        
        itRejects({sch : {tbl: null}}, "EXPECTED_ARRAY_OR_HASH_OR_STRING");
        itRejects({sch : {tbl: []}}, "EXPECTED_ARRAY_HAD_SOME_ELEMENTS");
        itRejects({"sch.sub" : {tbl: null}}, "EXPECTED_ARRAY_OR_HASH_OR_STRING");
        itRejects({"sch.sub" : {tbl: []}}, "EXPECTED_ARRAY_HAD_SOME_ELEMENTS");
        itRejects({"sch.sub.sub2" : {tbl: null}}, "EXPECTED_ARRAY_OR_HASH_OR_STRING");
        itRejects({"sch.sub.sub2" : {tbl: []}}, "EXPECTED_ARRAY_HAD_SOME_ELEMENTS");
        
        itCreates({sch: {tbl : "col"}}, "CREATE TABLE tbl (col)");
        _.each(TypeName.TYPES, function(type) {
            itCreates({sch: {tbl : {col: type}}}, "CREATE TABLE tbl (col "+type+")");    
        });
        
        itCreates({sch: {tbl : {col: "double"}}}, "CREATE TABLE tbl (col DOUBLE)");
        itCreates({sch: {tbl : {col: "DOUBLE  PRECISION"}}}, "CREATE TABLE tbl (col DOUBLE PRECISION)");
        itCreates({sch: {tbl : {col: "PRIMARY KEY"}}}, "CREATE TABLE tbl (col PRIMARY KEY)");
        //itCreates({sch: {tbl : {col: "CONSTRAINT cnst PRIMARY KEY"}}}, "CREATE TABLE tbl (col CONSTRAINT cnst PRIMARY KEY)");
        itCreates({sch: {tbl : {col: ["int", "DEFAULT 6"]}}}, "CREATE TABLE tbl (col INT DEFAULT 6)");
    
//        
//        it('supports {tbl : ["col", "PRIMARY KEY"]}', function() {
//            var input = {tbl : ["col", "PRIMARY KEY"]};
//            var schemata = enscript.schemataFrom(input);
//            var sql = schemata.tbl.enscriptCreate();
//            expect(sql).toEqual("CREATE TABLE tbl (id PRIMARY KEY)");
//        });
//        
//        it('supports {tbl : ["col", "PRIMARY  KEY"]}', function() {
//            var input = {tbl : ["col", "PRIMARY  KEY"]};
//            var schemata = enscript.schemataFrom(input);
//            var sql = schemata.tbl.enscriptCreate();
//            expect(sql).toEqual("CREATE TABLE tbl (id PRIMARY KEY)");
//        });
//
//        it("rejects null", function() {
//            expect(function() {
//                enscript.schemataFrom(null).enscript();
//            }).toThrow(new Error("cannot build column from null"));
//        });
//        
//        it("rejects ''", function() {
//            expect(function() {
//                enscript.columnFrom("").enscript();
//            }).toThrow(new Error("column must have a name"));
//        });
//        
//        it("rejects []", function() {
//            expect(function() {
//                enscript.columnFrom([]).enscript();
//            }).toThrow(new Error("expected one or more elements"));
//        });
//        
//        it("supports 'id'", function() {
//            var mdl = enscript.columnFrom("id");
//            var sql = mdl.enscript();
//            expect(sql).toEqual("id");
//        });
//        
//        it("supports ['id']", function() {
//            var mdl = enscript.columnFrom(["id"]);
//            var sql = mdl.enscript();
//            expect(sql).toEqual("id");
//        });
//        
//        it("supports ['id','int']", function() {
//            var mdl = enscript.columnFrom(["id","int"]);
//            var sql = mdl.enscript();
//            expect(sql).toEqual("id INT");
//        });
//  
//        it("supports 'PRIMARY KEY NOT NULL UNIQUE'", function() {
//            var mdls = enscript.columnConstraintsFrom([
//                "PRIMARY KEY",
//                "NOT NULL",
//                "UNIQUE"
//                ]);
//            
//            expect(mdls.length).toEqual(3);
//            var sql = mdls[0].enscript();
//            expect(sql).toEqual("PRIMARY KEY");
//            sql = mdls[1].enscript();
//            expect(sql).toEqual("NOT NULL");
//            sql = mdls[2].enscript();
//            expect(sql).toEqual("UNIQUE");
//        });
    });
    
    
});