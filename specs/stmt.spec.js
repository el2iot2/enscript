/*global describe:true it:true expect:true */

//describe("statements", function () {
//    var stmt = require('../lib/model/sql-stmt.js');
//    describe("create-table-stmt", function () {
//        it("supports a single attribute table", function() {
//            
//            function columns() {
//                this.DEF("id");
//            }
//                
//            var sql = stmt.CREATE_TABLE("foo", columns).enscript();
//            
//            expect(sql).toEqual("CREATE TABLE foo (id)");
//        });
//        
//         it("supports a single attribute table with db name", function() {
//            
//            function columns() {
//                this.DEF("id");
//            }
//                
//            var sql = stmt.CREATE_TABLE("db.foo", columns).enscript();
//            
//            expect(sql).toEqual("CREATE TABLE db.foo (id)");
//        });
//        
//        it("throws on duplicate column names", function() {
//            
//            function columns() {
//                this.DEF("id");
//                this.DEF("id2");
//                this.DEF("id");
//                this.DEF("id3");
//                this.DEF("id2");
//            }
//                                
//            expect(function() {stmt.CREATE_TABLE("foo", columns).enscript();})
//                .toThrow(new Error("duplicate column names: id,id2"));
//        });
//    });
//    
//    describe("column-constraint", function () {
//        var ColumnConstraint = require('../lib/model/column-constraint.js');
//
//        //Closure for a create script test
//        function addTests(funcName) {
//            var noUnderscores = funcName.replace(/_/g, " ");
//            it("supports constraint "+noUnderscores, function() {
//                var sql = ColumnConstraint[funcName]().enscript();
//                expect(sql).toEqual(noUnderscores);
//            });
//            
//            it("supports named constraint "+noUnderscores, function() {
//                var sql = ColumnConstraint["CONSTRAINT_"+funcName]("cn").enscript();
//                expect(sql).toEqual("CONSTRAINT cn "+noUnderscores);
//            });
//        }
//        
//        for (var key in ColumnConstraint) {
//            if (ColumnConstraint.hasOwnProperty(key) && key.lastIndexOf("CONSTRAINT", 0) < 0) {
//                addTests(key);
//            }
//        }
//    });
//    
//    describe("sql-stmt-list", function () {
//        var SqlStmtList = require('../lib/model/sql-stmt-list.js');
//
//        it("supports a no-op", function() {
//            var sql = new SqlStmtList().enscript();
//            expect(sql).toEqual("");
//        });
//        
//        it("supports a single statement", function() {
//            
//            function statements () {
//                function columns() {
//                    this.DEF("id");
//                }
//                this.CREATE_TABLE("foo", columns);
//            }
//            
//            var sql = new SqlStmtList(statements).enscript();
//            
//            expect(sql).toEqual("CREATE TABLE foo (id);");
//        });
//    });
//    
//    describe("type-name", function () {
//        var TypeName = require('../lib/model/type-name.js');
//
//        it("detects a missing type", function() {
//            var typeName = TypeName.typeNameFrom("");
//            expect(typeName).toBeNull();
//        });
//    });
//});